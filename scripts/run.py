import numpy as np
import torch
import argparse
import json
from mbdp.Methods import mb_vi, mb_mpi, optimal_cost, bellman_batches
from mbdp.Environments import Maze, FrozenLake, Taxi

parser = argparse.ArgumentParser(description='Mini-Batch Value Iteration.')

parser.add_argument("-env", type=str, default="maze")
parser.add_argument("-method", type=str, default="vi")
parser.add_argument("-grid_size", type=int, default=50)
parser.add_argument("-m", type=int, default=32)
parser.add_argument("-K", type=int, default=100)

args = parser.parse_args()

available_env = ['maze', 'frozenlake', 'taxi']
available_methods = ['vi', 'mpi']

if args.env not in available_env:
    raise ValueError("{} env is not supported.".format(args.env))
if args.method not in available_methods:
    raise ValueError("{} method is not supported".format(args.method))

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
tensor_type = torch.float64

print("device {}".format(device))

""" MDP structure"""
discount_factor = 0.95

if args.env == "maze":
    env = Maze(args.grid_size, discount_factor, 1, "uniform", device, tensor_type)
elif args.env == "frozenlake":
    env = FrozenLake(discount_factor, device, tensor_type)
else:
    env = Taxi(discount_factor, device, tensor_type)

J_star = optimal_cost(env)
import pdb; pdb.set_trace()
if args.m > env.N:
    mini_batch = env.N
else:
    mini_batch = args.m

benchmarks = []

if args.method == "vi":
    benchmarks.append(mb_vi(env, mini_batch, J_star, verbose=True))
else:
    print("mpi")
    benchmarks.append(mb_mpi(env, mini_batch, J_star, K=args.K, tol=10**-4, verbose=True))

torch.cuda.empty_cache()

res = {"mini-batch": mini_batch, "benchmarks": benchmarks}

if args.method == "vi":
    if args.env == "maze":
        file_name = '{}_{}_{}.json'.format(args.env, args.grid_size, args.method)
    else:
        file_name = '{}_{}.json'.format(args.env, args.method)
else:
    if args.env == "maze":
        file_name = '{}_{}_{}_K_{}.json'.format(args.env, args.grid_size, args.method, args.K)
    else:
        file_name = '{}_{}_K_{}.json'.format(args.env, args.method, args.K)
#save results
with open(file_name, 'w') as f:
    json.dump(res, f)



