from setuptools import setup, find_packages

setup(
   name='mbdp',
   version='0.1.0',
   author='Matilde Gargiani',
   author_email='gmatilde@ethz.ch',
   url='https://github.com/gargiani/MiniBatch-DPOperator',
   license='LICENSE.txt',
   description='Flexible and Parallel Dynamic Programming via the Mini-Batch DP Operator',
   long_description=open('README.md').read(),
   packages=['mbdp'],
   install_requires=["gym"]
)