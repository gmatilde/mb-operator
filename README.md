# MiniBatch-DPOperator

The Bellman operator constitutes the foundation of dynamic programming (DP). An alternative is presented by the Gauss-Seidel operator, whose evaluation, differently from that of the Bellman operator where the states are all processed at once, consists in updating one state at a time, while incorporating into the computation the interim results. The provably better convergence rate of DP methods based on the Gauss-Seidel operator comes at the price of an inherent sequentiality, which prevents the exploitation of modern multi-core systems. In this work we propose a new operator for dynamic programming, namely, the _randomized mini-batch operator_, which aims at realizing the trade-off between the faster convergence of the methods based on the Gauss-Seidel operator and the parallelization capability offered by the Bellman operator. After the introduction of the new operator, a theoretical analysis for validating its fundamental properties is conducted. Such properties allow one to successfully deploy the new operator in the main dynamic programming schemes, such as value iteration and modified policy iteration. We then compare the convergence of the DP algorithm based on the new operator with that of the counterparts, shedding lights on the algorithmic advantages of the new formulation and the impact of the batch-size parameter on the convergence. Finally, an extensive numerical evaluation of the newly introduced operator is conducted when used in the value iteration and modified policy iteration methods. In accordance with the theoretical derivations, the numerical results show the competitive performance of the proposed operator and its superior flexibility, which allows one to adapt the efficiency of its iterations to different structures of MDPs and hardware setups. See https://arxiv.org/abs/2110.02901 for more details!

## Installation

step 0: create a conda environment with the following command

$ conda create -n yourenvname python=3.7 anaconda

step 1: activate the created conda environment

$ source activate yourenvname

step 2: install pytorch with cuda

$ conda install pytorch torchvision torchaudio cudatoolkit -c pytorch -c nvidia

step 3: install the mbdp package 

- download this repo
- ```cd``` into it
- $ python setup.py install

step 4: test your installation

- cd into the folder ```scripts```
- $ python run.py

## Usage
The script ```run.py``` offers an example on how to deploy this python package. 

## Citation

@article{Gargiani2021,
    year      = {2021},
    publisher = {ArXiv},
    author    = {Matilde Gargiani and Andrea Martinelli and Max R. Martinez and John Lygeros},
    title     = {Parallel and Flexible Dynamic Programming via the Mini-Batch Bellman Operator}}

