import random
import torch
import numpy as np

def optimal_cost(env, random_seed=10, tol=10**-8):

    #set the random seed
    random.seed(random_seed)
    torch.manual_seed(random_seed)

    # Initialize the greedy policy with a random policy
    mu = torch.randint(0, len(env.U), (env.N, ), device=env.device)
    
    #Initialization
    J_mu = torch.zeros((env.N), device=env.device, dtype=env.tensor_type)
    J_old = J_mu.clone()

    I = torch.eye(env.N, device=env.device, dtype=env.tensor_type)

    iter = 0

    S = [i for i in range(env.N)]

    while torch.max(torch.abs(J_old - J_mu))>tol or iter==0:

        J_old = J_mu.clone()

        Jac = I - env.alpha * env.Piuj[S, mu[S], :]

        """Policy Evaluation """
        J_mu = torch.matmul(torch.inverse(Jac), env.G[S, mu[S]])  

        """ Policy improvement """
        mu = torch.argmin(env.G + env.alpha * torch.matmul(env.Piuj, J_mu), dim=1)
        print(mu, J_mu)

        iter += 1

        print("iter {}, gap {}".format(iter, torch.max(torch.abs(J_old - J_mu))))

    return J_mu

def mb_mpi(env, m, J_star, random_seed = 10, warmStarting=True, K=10, tol=10**-3, verbose=False):

    #set the random seed
    random.seed(random_seed)
    torch.manual_seed(random_seed)

    # Initialize the greedy policy with a random policy
    mu = torch.randint(0, len(env.U), (env.N, ), device=env.device)

    errors = []
    delta_times = []

    #Initialize the number of total MPI iterations
    mpi_iterations = 0
    
    #Initialization
    J_mu = torch.zeros((env.N), device=env.device, dtype=env.tensor_type)
    BJ_mu = torch.zeros((env.N), device=env.device, dtype=env.tensor_type)  

    #Initialize the S 
    S = [i for i in range(env.N)]
    n_iters = int(np.ceil(len(S)/m))

    while torch.max(torch.abs(J_star - J_mu))>tol:

        # When not using warm starting initialize every iteration the cost function
        if not warmStarting:
            #Re-Initialization
            J_mu = torch.zeros((env.N), device=env.device, dtype=env.tensor_type)
            BJ_mu = torch.zeros((env.N), device=env.device, dtype=env.tensor_type)

        epoch = 0

        mpi_iterations += 1

        # Initialize the time that records the total computation time of K-epochs
        t_comp = 0

        """ Approximate Policy Evaluation """
        while epoch <= K:

            epoch += 1

            iter = 0

            #shuffle S
            random.shuffle(S)

            """ New epoch """
            while iter < n_iters:
                                
                W = S[iter*m:min((iter+1)*m,len(S))]

                # To record the copmutation time
                torch.cuda.synchronize()
                start = torch.cuda.Event(enable_timing=True, blocking=True)
                end = torch.cuda.Event(enable_timing=True, blocking=True)
                start.record()

                # Update batch
                BJ_mu[W] = env.G[W, mu[W]] + env.alpha * torch.matmul(env.Piuj[W, mu[W], :], J_mu)

                end.record()
                torch.cuda.synchronize()

                t_comp += start.elapsed_time(end)

                J_mu[W] = BJ_mu[W]

                iter += 1

            
        delta_times.append(t_comp)    
        error = torch.max(torch.abs(J_star - J_mu))
        errors.append(error.item())
        
        if verbose:
            print("iteration {}, error {}".format(mpi_iterations, torch.max(torch.abs(J_star - J_mu))))
        
        """ Policy improvement """
        mu = torch.argmin(env.G + env.alpha * torch.matmul(env.Piuj, BJ_mu), dim=1)
    
    return {"errors": errors, "delta_times": delta_times}

def mb_vi(env, m, J_star, randomized=True, random_seed=10, tol=.0001, verbose=False, recovery=False):

    #set the random seed
    random.seed(random_seed)

    delta_times = []
    errors = []

    #Initialization
    J = torch.zeros((env.N), device=env.device, dtype=env.tensor_type)
    BJ = torch.zeros((env.N), device=env.device, dtype=env.tensor_type)   
    
    epoch = 0

    #Initialize the S 
    S = [i for i in range(env.N)]
    n_iters = int(np.ceil(len(S)/m))

    """ While cost function has not converge, keep applying the operator to the cost function """
    while True:

        epoch += 1

        iter = 0

        # Initialize the time that records the computation time per epoch
        t_comp = 0

        #shuffle S
        if randomized:
            random.shuffle(S) 

        """ New epoch """
        while iter < n_iters:
            
            W = S[iter*m:min((iter+1)*m,len(S))]

            # To record the copmutation time
            torch.cuda.synchronize()
            start = torch.cuda.Event(enable_timing=True, blocking=True)
            end = torch.cuda.Event(enable_timing=True, blocking=True)
            start.record()

            # Update batch
            BJ[W] = torch.min(env.G[W, :] + env.alpha * torch.matmul(env.Piuj[W, :, :], J),
                              1).values

            end.record()
            torch.cuda.synchronize()
            t_comp += start.elapsed_time(end)

            J[W] = BJ[W]

            iter += 1

        delta_times.append(t_comp)
        error = torch.max(torch.abs(J_star - J))
        errors.append(error.item())

        if verbose:
                print("epoch:{}, error: {}".format(epoch, error.item()))

        if error < tol and not recovery:
            return {"errors": errors, "delta_times": delta_times}
        elif error < tol and recovery:
            return {"errors":errors, "delta_times": delta_times, "smaller_batch":m}

def bellman_batches(env, m, J_star, randomized=True, random_seed=10, tol=.0001, verbose=False):

    #set the random seed
    random.seed(random_seed)

    delta_times = []
    errors = []

    #Initialization
    J = torch.zeros((env.N), device=env.device, dtype=env.tensor_type)
    BJ = torch.zeros((env.N), device=env.device, dtype=env.tensor_type)   
    
    epoch = 0

    #Initialize the S 
    S = [i for i in range(env.N)]
    n_iters = int(np.ceil(len(S)/m))

    """ While cost function has not converge, keep applying the operator to the cost function """
    while True:

        epoch += 1

        iter = 0

        # Initialize the time that records the computation time per epoch
        t_comp = 0

        #shuffle S
        if randomized:
            random.shuffle(S)        

        """ New epoch """
        while iter < n_iters:
            
            W = S[iter*m:min((iter+1)*m,len(S))]

            # To record the copmutation time
            torch.cuda.synchronize()
            start = torch.cuda.Event(enable_timing=True, blocking=True)
            end = torch.cuda.Event(enable_timing=True, blocking=True)
            start.record()

            # Update batch
            BJ[W] = torch.min(env.G[W, :] + env.alpha * torch.matmul(env.Piuj[W, :, :], J),
                              1).values

            end.record()
            torch.cuda.synchronize()
            t_comp += start.elapsed_time(end)

            iter += 1
        #update the cost once all the states have been processed
        J[W] = BJ[W]

        delta_times.append(t_comp)
        error = torch.max(torch.abs(J_star - BJ))
        errors.append(error.item())

        if verbose:
                print("epoch:{}, error: {}".format(epoch, error.item()))

        if error < tol:
            return {"errors": errors, "delta_times": delta_times, "smaller_batches": m}

if __name__ == '__main__':

    from Environments import Taxi

    size = 30
    alpha  = 0.95
    step = 1
    mode = "maze"
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    tensor_type = torch.float32

    taxi = Taxi(alpha, device, tensor_type)

    J_star = optimal_cost(taxi)

    mini_batch = 64

    res_VI = mb_vi(taxi, mini_batch, J_star, verbose=True)
    
    res_MPI = mb_mpi(taxi, mini_batch, J_star, K=50, verbose=True)
