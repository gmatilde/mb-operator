import numpy as np
import torch
import gym
import random

class Taxi:
    def __init__(self, alpha, device, tensor_type):

        self.device = device
        self.tensor_type = tensor_type
    
        env = gym.make("Taxi-v3")
        #extract the number of states
        self.N = 500
        #extract the number of actions
        nA = 6
        #discount factor
        self.alpha = torch.tensor(alpha, device=device, dtype=tensor_type)
        #initialize the 3D matrix P
        self.Piuj = torch.zeros((self.N, nA, self.N), dtype=tensor_type)
        #initialize the 2D matrix G
        self.G = torch.zeros((self.N, nA), dtype=tensor_type)

        self.U = []

        for a in range(nA):
            self.U.append(a)

        for s in range(self.N):
            for a in range(nA):
                for ii in env.P[s][a]:
                    self.Piuj[s, a, ii[1]] += ii[0]
                    if ii[3]: 
                        self.G[s, a] = 0.0
                    else:
                        self.G[s, a] = -ii[2]
                        
        self.Piuj = self.Piuj.to(device)
        self.G = self.G.to(device)

class FrozenLake:
    def __init__(self, alpha, device, tensor_type, penalty=10**3):

        self.device = device
        self.tensor_type = tensor_type

        self.letal_penalty = penalty

        env = gym.make("FrozenLake8x8-v1")
        #extract the number of states
        self.N = 64
        #extract the number of actions
        nA = 4
        #discount factor
        self.alpha = torch.tensor(alpha, device=device, dtype=tensor_type)

        #initialize the 3D matrix P
        self.Piuj = torch.zeros((self.N, nA, self.N), dtype=tensor_type)
        #initialize the 2D matrix G
        self.G = torch.zeros((self.N, nA), dtype=tensor_type)

        self.U = []

        for a in range(nA):
            self.U.append(a)

        for s in range(self.N):
            for a in range(nA):
                for ii in env.P[s][a]:
                    self.Piuj[s, a, ii[1]] += ii[0]
                    if ii[3]: 
                        if ii[2]==0.0:
                            self.G[s, a] = self.letal_penalty
                        else:
                            self.G[s, a] = 0.0
                    else:
                        self.G[s, a] = 1.0
        
        self.Piuj = self.Piuj.to(device)
        self.G = self.G.to(device)

class State:
    def __init__(self, i, coor, N):
        self.i = i
        self.coor = coor
        self.pos = np.array([((coor[0]+0.5)/N-0.5)*8,((coor[1]+0.5)/N-0.5)*8])

class Maze:
    def __init__(self, grid_size, alpha, step_size, structure, device, tensor_type, random_seed=10):

        self.device = device
        self.tensor_type = tensor_type

        """ Env characteristics"""
        self.grid_size = grid_size          # Grid size
        self.alpha = torch.tensor(alpha, device=device, dtype=tensor_type)    # Discount factor
        self.states = []                    # State space
        self.U = []                         # Control space
        self.grid = np.zeros((self.grid_size, self.grid_size))  # 2D array representing the maze

        #set the random seed
        np.random.seed(random_seed)

        """ Definal all possible controls with the given step size"""
        u_to_jump = {}  # Maps the index of control u to the delta in the coordinate space of the grid
        u = 0
        for i in range(-step_size, step_size+1):
            for j in range(-step_size, step_size+1):
                if not (i == 0 and j == 0) and abs(i) + abs(j) <= step_size:
                    u_to_jump[u] = (i, j)
                    self.U.append(u)
                    u+=1

        """Buid maze"""
        self.coor_to_state = {}     # Maps a coordinate in the grid space to the index of a state
        i = 0
        for row in range(grid_size):
            for col in range(grid_size):

                # Define walls of grid
                if not(row == self.grid_size//2 and 2<=col<=self.grid_size-1) and not(row == self.grid_size//4 and 0<=col<=self.grid_size-3) and not(row == self.grid_size//(4/3)and 0<=col<=self.grid_size-3) :
                    s = State(i, (row, col), self.grid_size)
                    self.states.append(s)
                    self.coor_to_state[s.coor] = s
                    self.grid[row, col] = 1
                    i+=1

        self.N = i
        self.r = 0
        self.G = np.zeros((self.N, len(self.U)))
        self.Piuj = np.zeros((self.N, len(self.U), self.N))

        for i in range(self.N):

            """ When the step size is larger than one it might be illegal to take certain controls due to the walls of 
            the maze. Therefore, the following block of returns all the legal controls that can be applied when starting
             at state i. This is done using the path-planning algorithm of BFS"""
            Q = [(self.states[i].coor, 0)]
            V = [self.states[i].coor]
            while len(Q) > 0 and Q[0][1] <= step_size:
                coor, dist = Q[0]
                Q = Q[1:]
                for step in [(0, -1), (-1, 0), (0, 1), (1, 0)]:
                    try_coor = (coor[0] + step[0], coor[1] + step[1])
                    if 0 <= try_coor[0] < self.grid_size and 0 <= try_coor[1] < self.grid_size and self.grid[
                        try_coor[0], try_coor[1]] == 1 and try_coor not in V:
                        Q.append((try_coor, dist + 1))
                        V.append(try_coor)

            for u in self.U:
                new_coor = (self.states[i].coor[0] + u_to_jump[u][0],self.states[i].coor[1] + u_to_jump[u][1])

                # If control u is legal, then when applying this control jump to j with prob 1
                if new_coor in V:
                    j = self.coor_to_state[new_coor].i
                    self.Piuj[i, u, j] = 1
                # Otherwise, stay in the same state with probability 1
                else:
                    self.Piuj[i, u, i] = 1
                # If we are in the terminal state the stage cost is 0
                if i == self.r:
                    self.G[i,u] == 0
                # Otherwise set the stage cost to be the l-2 norm distance traveled
                else:
                    self.G[i, u] = abs(u_to_jump[u][0])*1 + abs(u_to_jump[u][1])*1

        self.G = torch.tensor(self.G, device=device, dtype=tensor_type)

        if structure == "maze":
            pass

        if structure == "uniform":
            self.Piuj = np.random.uniform(0,1,(self.N,len(self.U), self.N))            
            # Normalize
            for i in range(self.N):
                for u in range(len(self.U)):
                    self.Piuj[i,u,:] /= np.sum(self.Piuj[i,u,:])

        if structure == "onehot":
            self.Piuj = np.zeros((self.N, len(self.U), self.N))
            for i in range(self.N):
                for u in range(len(self.U)):
                    j = np.random.randint(0, self.N)
                    self.Piuj[i,u,j] = 1

        self.Piuj = torch.tensor(self.Piuj, device=device, dtype=tensor_type)


if __name__ == '__main__':

    size = 80
    alpha  = 0.95
    step = 1
    mode = "uniform"
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    tensor_type = torch.float32

    maze = Maze(size, alpha, step, mode, device, tensor_type)

    frozenlake = FrozenLake(alpha, device, tensor_type)

    taxi = Taxi(alpha, device, tensor_type)
    